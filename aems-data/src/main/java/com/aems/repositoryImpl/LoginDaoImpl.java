package com.aems.repositoryImpl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.aems.model.Login;
import com.aems.model.Registration;
import com.aems.repository.LoginDao;
import com.aems.util.IConstant;
@Repository
public class LoginDaoImpl implements LoginDao {

	@Autowired
	private HibernateTemplate hibernateTemplate;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Object> getUserInfo(String id, String password) {
		
		List<Object> login = null;
		System.out.println(hibernateTemplate);
		
		 login = hibernateTemplate.find("from Login login where login.emailId = ? and login.password = ? and login.isDeleted=?", id, password,IConstant.IS_DELETED);
	   

		return login;
	}

}
