package com.aems.repositoryImpl;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;



import  com.aems.repository.ForgotService;
import com.aems.util.Encryption;

@Service
public class ForgotServiceImpl implements ForgotService {
	
	@Autowired
	private JavaMailSender mailSender;

	/**
	 * @param mailSender
	 *            the mailSender to set
	 */
	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}
	
	
	public void sendConfirmation(final String pass, final String emailId,final String name) {
    final String password = Encryption.decrypt(pass);
    System.out.println("password is--" + password);
    
    
    MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {
    
    	public void prepare(MimeMessage mimeMessage) throws Exception {
            mimeMessage.setRecipient(Message.RecipientType.TO,new InternetAddress(emailId));
        String message = "Dear "+name+",<br>";
        message += "&nbsp;&nbsp;&nbsp;You have registered with Aartek successfully, Please find your log-in credentials below::<br>";
        message += "Email ID /Username : "+emailId+"<br>";
        message +="Password : "+password+"<br>";
        mimeMessage.setFrom(new InternetAddress("kapil.jain390@gmail.com"));
        mimeMessage.setSubject("Welcome to Aartek");
        mimeMessage.setContent(message,"text/html");
      }
    	
    };
    mailSender.send(messagePreparator);
  }

}
