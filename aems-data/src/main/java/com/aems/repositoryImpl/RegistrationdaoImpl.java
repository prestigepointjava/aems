package com.aems.repositoryImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.aems.model.Registration;
import com.aems.repository.RegistrationDao;
import com.aems.util.IConstant;

@Repository
public class RegistrationdaoImpl implements RegistrationDao {

	@Autowired
	private HibernateTemplate hibernateTemplate;

	@Override
	public boolean addEmployeeInformation(Registration registration) {
		
		List content = null;
		content = hibernateTemplate.find("from Registration where login.emailId='"
						+ registration.getLogin().getEmailId()
						+ "' AND IS_DELETED=" + IConstant.IS_DELETED);
		if (content.isEmpty()) {
			try {
				hibernateTemplate.save(registration);
				return true;
			} catch (Exception e) {
				System.out.println("Inside Registration Repository Exception Block"+ e);
			}
		}
		
		
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Registration> getAllEmployeesDetails() {
		 List<Registration> list = null;
		    list = hibernateTemplate.find("from Registration reg where reg.isDeleted=?",IConstant.IS_DELETED);
		    return list;
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object> getEmployeeInfoById(Integer registrationId) {
		  List<Object> list = null;
		    list = hibernateTemplate.find("from Registration reg where reg.id=" + registrationId);
		  
		   return list;
		}

	@Override
	public boolean updateEmployeeInformation(Registration registration) {
		List content = null;
		content = hibernateTemplate.find("from Registration where login.emailId='"
				+ registration.getLogin().getEmailId()
				+ "' AND IS_DELETED=" + IConstant.IS_DELETED);
		if (!content.isEmpty()) {
			try {
				hibernateTemplate.update(registration);
				return true;
			} catch (Exception e) {
				System.out.println("Inside Registration Repository Exception Block"+ e);
			}
		}
		
		
		
		return false;
	}

	@Override
	public void deleteEmployee(Long employeeId) {
		System.out.println(employeeId);
		Registration registration = (Registration) hibernateTemplate.get(Registration.class, employeeId);
		registration.setIsDeleted(IConstant.IS_DELETED_DEACTIVE);
		registration.getLogin().setIsDeleted(IConstant.IS_DELETED_DEACTIVE);
		
		if (null != registration) {
		      hibernateTemplate.update(registration);
		    }
		System.out.println(registration);
	}

}
