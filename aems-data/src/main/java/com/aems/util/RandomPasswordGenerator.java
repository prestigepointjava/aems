package com.aems.util;

import java.util.Random;

import org.springframework.stereotype.Service;

@Service
public class RandomPasswordGenerator {

  // DATAS
    // characters with which the password will be composed
    private static final int charactersSize = 100;
    private static char [] characters = new char [charactersSize];

    // keep the counts of used characters
    private static int charactersCount = 0;

    // size of the password to generate
    private static int passwordSize;

    private static char [] initCharacters() {
      int i = 0;

      // add 0-9
      for ( int j = 48; j < 58; ++i, ++j, ++charactersCount ) {
        characters[i] = (char) j;
      }

     /* // add A-Z
      for ( int j = 97; j < 123; ++i, ++j, ++charactersCount ) {
        characters[i] = (char) j;
      }
*/
      return characters;
    }

    // generate a random password
    public static String get() {

    passwordSize = 5;
    initCharacters();
      // initialize the random number generator
      Random rnd = new Random();

      
     String password="";
      // choose a random character from the array
      for ( int i = 0; i < passwordSize; ++i ) {
    	  password=password+ characters[ rnd.nextInt(charactersCount) ];
     
      }
       
      return password;
    }
   
}