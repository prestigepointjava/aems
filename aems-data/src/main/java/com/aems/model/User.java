package com.aems.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
@Entity
@Table(name = "usertype")
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {

	@Id
	@Column(name = "USER_TYPE_ID")
	@GeneratedValue
	private long id;
		
	@Column(name="USER_TYPE")
	private String userType;

	@Column(name = "IS_DELETED")
	  private Integer isDeleted;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public Integer getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", userType=" + userType + ", isDeleted="
				+ isDeleted + "]";
	}
	
	
	
	
}
