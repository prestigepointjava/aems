package com.aems.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


import com.aems.model.Login;

@Entity
@Table(name = "login")
public class Login {

	@Id
	@Column(name = "LOGIN_ID")
	@GeneratedValue
	private long id;
		
	private String emailId;

	private String password;
    
	private Integer isDeleted;
	
	@Column(name="USER_TYPE_ID")
	private int userTypeId;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "USER_TYPE_ID",updatable=false,insertable=false)
	private User user;

	public int getUserTypeId() {
		return userTypeId;
	}



	public void setUserTypeId(int userTypeId) {
		this.userTypeId = userTypeId;
	}



	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}


	public String getEmailId() {
		return emailId;
	}


	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public Integer getIsDeleted() {
		return isDeleted;
	}


	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}



	public User getUser() {
		return user;
	}



	public void setUser(User user) {
		this.user = user;
	}



	@Override
	public String toString() {
		return "Login [id=" + id + ", emailId=" + emailId + ", password="
				+ password + ", isDeleted=" + isDeleted + ", userTypeId="
				+ userTypeId + ", user=" + user + "]";
	}



	

	
	
}