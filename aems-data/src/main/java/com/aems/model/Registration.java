package com.aems.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "registration")
public class Registration {
	@Id
	@Column(name = "ID")
	@GeneratedValue
	private long id;
		
	@Column(name="FIRST_NAME")
	private String fisrtName;
				   	

	@Column(name="LAST_NAME")
	private String lastName;


	@Column(name="ADDRESS")
	private String address;
	

	@Column(name="CONTACT")
	private String contact;
	

	@Column(name="GENDER")
	private String gender;
	

	@Temporal(TemporalType.DATE)
	@Column(name = "DOJ")
	private Date dateOfJoining;
	

	@Column(name="DESIGNATION")
	private String designation;
	

	@Column(name="PERSONAL_ID")
	private String personalId;
	

	@Column(name="MARITAL_STATUS")
	private String maritalStatus;
	
	@Column(name="TYPE_OF_EMPLOYEE")
	private String typeOfEmployee;
	
	@Column(name="SALARY")
	private Double salary;
	
	
	@Column(name="QULIFICATION")
	private String qualification;
	
	@Column(name = "IS_DELETED")
	private Integer isDeleted;
	
	

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "LOGIN_ID")
	private Login login;



	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public String getFisrtName() {
		return fisrtName;
	}



	public void setFisrtName(String fisrtName) {
		this.fisrtName = fisrtName;
	}



	public String getLastName() {
		return lastName;
	}



	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	public String getAddress() {
		return address;
	}



	public void setAddress(String address) {
		this.address = address;
	}



	public String getContact() {
		return contact;
	}



	public void setContact(String contact) {
		this.contact = contact;
	}



	public String getGender() {
		return gender;
	}



	public void setGender(String gender) {
		this.gender = gender;
	}



	public Date getDateOfJoining() {
		return dateOfJoining;
	}



	public void setDateOfJoining(Date dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}



	public String getDesignation() {
		return designation;
	}



	public void setDesignation(String designation) {
		this.designation = designation;
	}



	public String getPersonalId() {
		return personalId;
	}



	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}



	


	public String getMaritalStatus() {
		return maritalStatus;
	}



	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}



	public String getTypeOfEmployee() {
		return typeOfEmployee;
	}



	public void setTypeOfEmployee(String typeOfEmployee) {
		this.typeOfEmployee = typeOfEmployee;
	}



	public Double getSalary() {
		return salary;
	}



	public void setSalary(Double salary) {
		this.salary = salary;
	}



	public String getQualification() {
		return qualification;
	}



	public void setQualification(String qualification) {
		this.qualification = qualification;
	}



	public Integer getIsDeleted() {
		return isDeleted;
	}



	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}



	public Login getLogin() {
		return login;
	}



	public void setLogin(Login login) {
		this.login = login;
	}



	@Override
	public String toString() {
		return "Registration [id=" + id + ", fisrtName=" + fisrtName
				+ ", lastName=" + lastName + ", address=" + address
				+ ", contact=" + contact + ", gender=" + gender
				+ ", dateOfJoining=" + dateOfJoining + ", designation="
				+ designation + ", personalId=" + personalId
				+ ", maritualStatus=" + maritalStatus + ", typeOfEmployee="
				+ typeOfEmployee + ", salary=" + salary + ", qualification="
				+ qualification + ", isDeleted=" + isDeleted + ", login="
				+ login + "]";
	}

	

			
	}