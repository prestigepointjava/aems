package com.aems.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="attendance")
public class Attendance implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue
	private Long id;
	
	

	@Column(name="DATE")
	private String date;
	
	@Column(name="EMPLOYEE_ID")
	private Long employeeId;
	
	@Column(name="STATUS")
	private Integer status;

	@Column(name="REMARK")
	private String remark;

	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}



	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getId() {
		return id;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Attendance [id=" + id + ", date=" + date + ", employeeId="
				+ employeeId + ", status=" + status + ", remark=" + remark
				+ "]";
	}	
	
	
	
}
