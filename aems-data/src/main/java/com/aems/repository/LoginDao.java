package com.aems.repository;

import java.util.List;


public interface LoginDao {
	List<Object> getUserInfo(String id, String password );
}
