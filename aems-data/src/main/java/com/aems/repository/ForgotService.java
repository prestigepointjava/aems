package com.aems.repository;

public interface ForgotService {
	  
  public void sendConfirmation(String password, String emailId,String name);
}
