package com.aems.repository;

import java.util.List;

import com.aems.model.Registration;

public interface RegistrationDao {
	public boolean addEmployeeInformation(Registration registration);
	public List<Registration> getAllEmployeesDetails();
	public List<Object> getEmployeeInfoById(Integer registrationId);
	public boolean updateEmployeeInformation(Registration registration);
	public void deleteEmployee(Long employeeId);
	
}
