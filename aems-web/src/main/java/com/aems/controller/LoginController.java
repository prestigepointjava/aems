sdfgdfgfggfdg
package com.aems.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aems.model.Attendance;
import com.aems.model.Login;
import com.aems.service.AttendanceService;
import com.aems.service.LoginService;
import com.aems.validator.SignInValidator;

@Controller
public class LoginController {
	
	@Autowired
	private LoginService loginService;
	
	@Autowired
	private AttendanceService attendanceService;
	

	  @Autowired
	  private SignInValidator signInValidator;

	
	@RequestMapping("/welcome")
	  public String login(Map<String, Object> map, Model model){
		
	    map.put("Login", new Login());
	
	    System.out.println("welcome");
	      return "welcomePage";
	  }
	
	 @RequestMapping(value = "/userSignIn", method = RequestMethod.POST)
	  public String signInAction(@ModelAttribute("Login") Login login, BindingResult result,
	      ModelMap model, Map<String, Object> map, HttpServletRequest request,
	      HttpServletResponse response) {
		
		 signInValidator.validate(login, result);
		    if (result.hasErrors()) {
		      return "welcomePage";
		    }
		login=loginService.getUserInfo(login);
		
	    if (login == null) {
	    	return "welcomePage";
	    }
	    else{
	    	
	      HttpSession session = request.getSession();
	      session.setAttribute("login", login);
	      boolean attendanceStatus=attendanceService.employeeLogIn(login);
	      
	     return "employeePage";  
	      
	    }
	  }
	 
	 @RequestMapping("/logout")
	  public String showLogout(Map<String, Object> map, Model model, HttpServletRequest request) {
	    HttpSession session = request.getSession();
	    map.put("Login", new Login());
	    model.addAttribute("Login", new Login());
	    session.invalidate();
	    return "welcomePage";
	  }
}