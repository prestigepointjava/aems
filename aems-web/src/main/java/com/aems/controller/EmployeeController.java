package com.aems.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aems.model.Registration;
import com.aems.validator.RegistrationValidator;

@Controller
public class EmployeeController {

	 @Autowired
	  private RegistrationValidator registrationValidator;
	
	@RequestMapping("/viewProfile")
	  public String viewProfile(Map<String, Object> map, ModelMap model){
		model.addAttribute("registration",new Registration());
		return "viewProfile";
	}
	
	@RequestMapping("/editProfile")
	  public String editProfile(Map<String, Object> map, ModelMap model){
		
		model.addAttribute("registration",new Registration());
		
		return "editProfile";
	}
	
	 @RequestMapping(value = "/editProfileDetail", method = RequestMethod.POST)
	  public String updateEmployee(@ModelAttribute("registration") Registration registration,
	     BindingResult result, ModelMap model, Map<String, Object> map, HttpServletRequest request) {
		 
		 HttpSession session = request.getSession();
		 String message="";  
		 boolean status = false;
		 registrationValidator.validate(registration, result);
		 if (result.hasErrors()) {
		      return "editProfile";
		 }
		
		 return "editProfile";
		 
	  }
	
}
