package com.aems.validator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.aems.model.Registration;
import com.aems.util.IConstant;

@Component
public class RegistrationValidator implements Validator {

  public boolean supports(Class<?> clazz) {
    return Registration.class.isAssignableFrom(clazz);
  }

  public void validate(Object target, Errors errors) {
    String regex = "(.)*(\\d)(.)*";
    Pattern pattern = Pattern.compile(regex);
    Registration userRegistration = (Registration) target;
   
    if (userRegistration.getTypeOfEmployee().equals("global")) {
        errors.rejectValue("typeOfEmployee", "error.typeOfEmployee.empty");
      }
    if (userRegistration.getDesignation().equals("global")) {
        errors.rejectValue("designation", "error.designation.empty");
      }
    if (userRegistration.getGender().equals("global")) {
        errors.rejectValue("gender", "error.gender.empty");
      }
        
    if (userRegistration.getLogin().getUserTypeId()==0) {
         errors.rejectValue("login.userTypeId", "error.userType.empty");
      }
  
       
    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fisrtName", "error.firstName.empty");
    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "error.lastName.empty");
    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "designation", "error.designation.empty");
    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "salary", "error.salary.empty");
    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "login.emailId", "error.email.empty");
    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address", "error.address.empty");

   
    if (userRegistration.getFisrtName() != null && userRegistration.getFisrtName()!= "") {
     
      }
    
    if (userRegistration.getLogin().getEmailId() != null&& userRegistration.getLogin().getEmailId().trim().length() > 0) {
      boolean b = ValidationUtil.validateEmail(userRegistration.getLogin().getEmailId());
      if (userRegistration.getLogin().getEmailId().contains("@") != true && !b) {
        errors.rejectValue("login.emailId", "error.email.first.rule");
      } else if (userRegistration.getLogin().getEmailId().contains(".com") != true
          && userRegistration.getLogin().getEmailId().contains(".net") != true && !b) {
        errors.rejectValue("login.emailId", "error.email.second.rule");
      } else if (!b) {
        errors.rejectValue("login.emailId", "error.email.required");
      }
    }

  }
}
