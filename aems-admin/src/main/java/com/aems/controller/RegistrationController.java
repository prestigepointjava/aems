package com.aems.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.aems.model.Login;
import com.aems.model.Registration;
import com.aems.service.LoginService;
import com.aems.service.RegistrationService;
import com.aems.validator.RegistrationValidator;

@Controller
public class RegistrationController {
	
	@Autowired
	private LoginService loginService;
	
	@Autowired
	private RegistrationService registrationService;

	 @Autowired
	  private RegistrationValidator registrationValidator;
	 
	 
	@RequestMapping("/viewEmployees")
	  public String viewEmployee(Map<String, Object> map, Model model,HttpServletRequest request,@RequestParam(required = false) String message){
		  HttpSession session = request.getSession();
		    Login loginMember = (Login) session.getAttribute("login");
		    if(loginMember!=null){
		    	
		    	      map.put("Registration", new Registration());
		    	      List<Registration> employeeList = null;
		    	      employeeList = registrationService.getAllEmployeesDetails();
		    	      if (employeeList != null) {
		    	        model.addAttribute("employeeList", employeeList);
		    	        model.addAttribute("message", message);
		    	      }
		    	     		    	    
		    	
		    	return "employeesDetails";
		    }
		    else {
		    	 model.addAttribute("Login", new Login());
		  	   
				 return "welcomePage";
			}
		    }
	 @RequestMapping(value = "/registerEmployee")
	  public String registerEmployee(Model model, Map<String, Object> map, @RequestParam(required = false) String successMsg,HttpServletRequest request) {
		
		 map.put("registration", new Registration());
		 model.addAttribute("successMsg", successMsg);
		 HttpSession session = request.getSession();
		 Login loginMember = (Login) session.getAttribute("login");
		    if(loginMember!=null){
		    	return "registration";
		    }
		    else {
		    	 model.addAttribute("Login", new Login());
		  	     return "welcomePage";
			}
		 
	   
	  }
	 @RequestMapping(value = "/addEmployee", method = RequestMethod.POST)
	  public String addEmployee(@ModelAttribute("registration") Registration registration,
	     BindingResult result, ModelMap model, Map<String, Object> map, HttpServletRequest request) {
		 
		 HttpSession session = request.getSession();
		  
		 boolean status = false;
		 String message="";
		 registrationValidator.validate(registration, result);
		 if (result.hasErrors()) {
		      return "registration";
		 }
		 status = registrationService.addEmployeeInformation(registration);
		 if (status){
			 message = "Employee registered with us successfully";
			 model.addAttribute("message",message);
			 model.addAttribute("registration", new Registration());
		     return "registration";
		 } else {
			 message = "Employee have already registered with us";
			 model.addAttribute("message",message);
			 model.addAttribute("registration", registration);
		     return "registration";
		    }
	    
	  }
	 
	 @RequestMapping(value = "/updateEmployeeDetail", method = { RequestMethod.GET, RequestMethod.POST })
	  public String updateEmployeeDetail(@ModelAttribute("registration") Registration registration, BindingResult result,
	      ModelMap model, HttpServletRequest request, @RequestParam(required = false) Integer registrationId) {
	    
		 HttpSession session = request.getSession();
		 Login loginMember = (Login) session.getAttribute("login");
		    if(loginMember!=null){
		    	 registration = registrationService.getEmployeeInfoById(registrationId);
		  	   				 
			    model.put("registration", registration);
			  
			    return "editEmployee";
		    }
		    else {
		    	 model.addAttribute("Login", new Login());
		  	     return "welcomePage";
			}
		 
		
	  }
	 
	 @RequestMapping(value = "/updateEmployee", method = RequestMethod.POST)
	  public String updateEmployee(@ModelAttribute("registration") Registration registration,
	     BindingResult result, ModelMap model, Map<String, Object> map, HttpServletRequest request) {
		 
		 HttpSession session = request.getSession();
		 String message="";  
		 boolean status = false;
		 registrationValidator.validate(registration, result);
		 if (result.hasErrors()) {
		      return "editEmployee";
		 }
		 
		 status = registrationService.updateEmployeeInformation(registration);
		 if(status){
			 message="Record updated successfully";
			
		  }
		 else{
			 message="Record not found";
			 					 
		 }
		 model.addAttribute("message",message);
		 model.addAttribute("registration", new Registration());
		 
		 return "editEmployee";
		 
	  }
	 
	 @RequestMapping(value = "/deleteEmployee", method = { RequestMethod.GET, RequestMethod.POST })
	  public String deleteEmployee(@ModelAttribute("registration") Registration registration,
	      BindingResult result, ModelMap model, HttpServletRequest request,
	      @RequestParam(required = false) Long registrationId) {
		 registrationService.deleteEmployee(registrationId);
	    model.addAttribute("message", "Employee deleted successfully!");
	    return "redirect:/viewEmployees.do";
	  }

}