<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=utf-8"
  pageEncoding="utf-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>



<html>
<head>

<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script>
  $(function() {
    $( "#datepicker" ).datepicker();
  });
  </script>
</head>
<body>
<div>
<jsp:include page="/WEB-INF/jsp/admin.jsp" />
<br/>
<br/>
<form:form action="addEmployee.do" modelAttribute="registration" method="POST" autocomplete="off" id="sign-gobal" class="sign-gobal">
    <h3 class="textDecorationCls">${message}</h3>
    <ul class="leftsidefrm" style="list-style: none;">
           
      <li>
      <form:input path="fisrtName"  placeholder="First Name" id="fld-a" maxlength="40"  required ="autofocus" />
       <form:input path="lastName" placeholder="Last Name" id="fld-b" maxlength="40"  required ="autofocus" />
      </li>
       <li>
       <form:errors path="fisrtName" cssClass="error"></form:errors>
       <form:errors path="lastName" cssClass="error"></form:errors>
       </li>
      <li>
      <li>
      
       <form:select path="designation" id="designation_list">
       <form:option value="global" label="Select designation of Employee" />
       <form:option value="Sr. Software Engineer"  label="Sr. Software Engineer" />
       <form:option value="Software Engineer"  label="Software Engineer" />
       <form:option value="Trainee"  label="Trainee" />
       <form:option value="Programmer Analyst"  label="Programmer Analyst" />
       <form:option value="System analyst"  label="System analyst" />
       <form:option value="Project Lead"  label="Project Lead" />
       <form:option value="Project manager"  label="Project manager" />
       <form:option value="Architect"  label="Architect" />
        <form:option value="Test lead"  label="Test lead" /> 
        <form:option value="Test engineer"  label="Test engineer" />
                  
      </form:select>
      </li>
      <li>
      <form:errors path="designation" cssClass="error" ></form:errors>
      
       </li>
        <li>
      <form:select path="typeOfEmployee" id="the_list">
       <form:option value="global" label="Select Type of Employee" />
       <form:option value="full time"  label="Full Time" />
       <form:option value="part time"  label="Part Time" />
       
      </form:select>
        <form:errors path="typeOfEmployee" cssClass="error"></form:errors>
      </li>
      <li>
       <form:select path="gender" id="gender_list">
       <form:option value="global" label="Select gender" />
       <form:option value="M"  label="Male" />
       <form:option value="F"  label="Female" />
       
      </form:select>
        <form:errors path="gender" cssClass="error"></form:errors>
      </li>
      
       <li>
      <form:select path="login.userTypeId" id="userTypeId_list">
       <form:option value="0" label="Select Type of User" />
       <form:option value="1"  label="Admin" />
       <form:option value="2"  label="Employee" />
       
      </form:select>
        <form:errors path="login.userTypeId" cssClass="error"></form:errors>
      </li>
      
     <li>
       <form:input path="dateOfJoining" id="datepicker" class="time ui-timepicker-input" placeholder="Date of Birth" required ="autofocus"  />
          
      </li>
      <li>
      <form:errors path="dateOfJoining" cssClass="error" ></form:errors>
   </li>
   <li>
      <form:input path="salary"  placeholder="Salary" id="fld-g" required ="autofocus" maxlength="50" />
     
      </li>
      <li>
      <form:errors path="salary" cssClass="error" ></form:errors>
      
       </li>
       <li>
      <form:input path="address"  name="address" placeholder="Address" required ="autofocus" id="fld-c" maxlength="150" />
      <form:errors path="address" cssClass="error"></form:errors>
      </li>
     
      <li>
      <form:input path="login.emailId"  placeholder="Email Address" id="fld-g" required ="autofocus" maxlength="50" />
     
      </li>
      <li>
      <form:errors path="login.emailId" cssClass="error" ></form:errors>
      
       </li>
      <li>
       <button type="submit" class="btn btn-primary btn-block" tabindex="4">Register Employee</button>
      </li>
      
      </ul>
      
      </form:form>
      </div> 
 </body>   
</html>