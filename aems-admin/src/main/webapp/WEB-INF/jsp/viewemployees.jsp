<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=utf-8"
  pageEncoding="utf-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>



<html>
<head>

<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>

</head>
<body>
<div>
<jsp:include page="/WEB-INF/jsp/admin.jsp" />
<br/>
<br/>
<h3 align="center" >${message}</h3>
<h3 align="center">Your Registered Employee List</h3>
<div class="table-contentmain">
<c:choose>
<c:when test="${!empty employeeList}">
<table border="0px" cellpadding="0" cellspacing="0" class="mid_table">
      <thead>
<tr>
<th class="sortable">S.NO</th>
<th class="thb">First NAME</th>
<th class="thb">Last NAME</th>
<th class="thb">CONTACT NO.</th>
<th class="thb">Designation</th>
<th class="thb">Salary</th>
<th class="thb">Email Id</th>
<th class="thb">EDIT</th>
<th class="thb">DELETE</th>
</tr>
</thead>
  <tbody>

  <c:forEach items="${employeeList}" var="registration">
 
  <tr>
   <td class="sortable">${registration.id}</td>
   <td class="thb">${registration.fisrtName}</td>
   <td class="thb">${registration.lastName}</td>
   <td class="thb">${registration.contact}</td>
   <td class="thb">${registration.designation}</td>
   <td class="thb">${registration.salary}</td>
   <td class="thb">${registration.login.emailId}</td>
   <td class="thb">
	   <a  href="updateEmployeeDetail.do?registrationId=${registration.id}">Edit</a>
   </td> 
   <td class="thb">
	   <a  href="deleteEmployee.do?registrationId=${registration.id}">Delete</a>
   </td> 
   </tr>
  </c:forEach>
  </tbody> 
  </table>
  </c:when>
<c:otherwise>
<c:out value="NO Employee Register , Please Register Employee "></c:out>
</c:otherwise>
</c:choose>
</div>
</div>
 </body>   
</html>