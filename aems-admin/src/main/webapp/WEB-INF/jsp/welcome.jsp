<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=utf-8"
  pageEncoding="utf-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>

<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
<script type="text/javascript" src="js/common.js"></script>
<link rel="stylesheet" type="text/css" href="css/style.css" media="screen">

</head>
<body>
 <div id="unpaid-page" class="unpaid-page">
      <!----------form----------id----------->
      <form:form method="POST" action="userSignIn.do" modelAttribute="Login" name="signup-page-frm" id="signup-page-frm" autocomplete="off" >
      <fieldset>
          <form:errors path="emailId" cssClass="error"></form:errors>
        <form:errors path="password" cssClass="error"></form:errors>
        <div class="fields">
    
          <form:input path="emailId" name="username" placeholder="Username" id="id_username" tabindex="1" required="autofocus" maxlength="50" />
          <form:password path="password"  name="password" placeholder="Password" id="id_password" tabindex="2" required="autofocus" maxlength="15" />
        </div>
        
        <button type="submit" class="btn btn-primary btn-block" tabindex="4">Log in</button>
      </fieldset>
  </form:form>     
</div>
</body>
</html>