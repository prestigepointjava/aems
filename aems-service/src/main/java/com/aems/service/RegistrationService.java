package com.aems.service;

import java.util.List;

import com.aems.model.Registration;

public interface RegistrationService {

	public boolean addEmployeeInformation(Registration registration);

	public List<Registration> getAllEmployeesDetails();

	public Registration getEmployeeInfoById(Integer registrationId);

	public boolean updateEmployeeInformation(Registration registration);

	public void deleteEmployee(Long registrationId);
	
	
	
}
