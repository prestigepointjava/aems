package com.aems.service;

import com.aems.model.Login;

public interface LoginService {
	
	public Login getUserInfo(Object object);
}
