package com.aems.service;

import com.aems.model.Login;

public interface AttendanceService {

	public boolean employeeLogIn(Login login);
	
}
