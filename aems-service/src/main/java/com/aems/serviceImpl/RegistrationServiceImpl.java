package com.aems.serviceImpl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aems.model.Registration;
import com.aems.repository.ForgotService;
import com.aems.repository.RegistrationDao;
import com.aems.service.RegistrationService;
import com.aems.util.IConstant;
import com.aems.util.RandomPasswordGenerator;

@Service
public class RegistrationServiceImpl implements RegistrationService{

	@Autowired 
	RegistrationDao registrationDao;
	
	@Autowired
	ForgotService forgotService;
	
	@Override
	public boolean addEmployeeInformation(Registration registration) {
		
	    
		registration.setIsDeleted(IConstant.IS_DELETED);
		String password=RandomPasswordGenerator.get();
	    registration.getLogin().setPassword(password);	    
	    registration.getLogin().setIsDeleted(IConstant.IS_DELETED);
	    
	   // forgotService.sendConfirmation(password, registration.getLogin().getEmailId(), registration.getFisrtName());
	    
		return registrationDao.addEmployeeInformation(registration);
	}

	@Override
	public List<Registration> getAllEmployeesDetails() {
		
		return registrationDao.getAllEmployeesDetails();
	}

	@Override
	public Registration getEmployeeInfoById(Integer registrationId) {
	
		    List<Object> list = new ArrayList<Object>();
		    Registration registration = new Registration();
		    list = registrationDao.getEmployeeInfoById(registrationId);
		    for (Object object : list) {
		      registration = (Registration) object;
		     
		    }
		    return registration;
	}

	@Override
	public boolean updateEmployeeInformation(Registration registration) {
		
		return registrationDao.updateEmployeeInformation(registration);
	}

	@Override
	public void deleteEmployee(Long employeeId) {
		
		registrationDao.deleteEmployee(employeeId);
	}

}
