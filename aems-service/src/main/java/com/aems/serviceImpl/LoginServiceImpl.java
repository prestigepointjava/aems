package com.aems.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aems.model.Login;
import com.aems.repository.LoginDao;
import com.aems.service.LoginService;
import com.aems.util.Encryption;

@Service
public class LoginServiceImpl implements LoginService {
	
	@Autowired
	private LoginDao loginDao;
	
	@Override
	public Login getUserInfo(Object login) {
		
	 Login loginMember = (Login) login;
	   // String password = Encryption.encrypt(loginMember.getPassword());
	    List<Object> memberList = null;
	     
	    if (login != null) {
	      if (loginMember.getEmailId() != null && loginMember.getPassword() != null) {
	        memberList = loginDao.getUserInfo(loginMember.getEmailId(), loginMember.getPassword());
	      }
	    }
	     if (memberList.size() == 0) {
	      loginMember = null;
	    } else {
	      loginMember = (Login) memberList.get(0);
	    }
	    return loginMember;
	  }

}
