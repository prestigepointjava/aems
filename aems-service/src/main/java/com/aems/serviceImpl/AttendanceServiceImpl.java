package com.aems.serviceImpl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aems.model.Attendance;
import com.aems.model.Login;
import com.aems.repository.AttendanceDao;
import com.aems.service.AttendanceService;
@Service
public class AttendanceServiceImpl implements AttendanceService {

	@Autowired
	private AttendanceDao attendanceDao;
	
	@Override
	public boolean employeeLogIn(Login login) {
		
		System.out.println(login);
		
		Attendance attendance=new Attendance();
		attendance.setEmployeeId(login.getId());
		attendance.setStatus(1);
		attendance.setRemark("Present");
		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		 //get current date time with Date()
		Date date = new Date();
		attendance.setDate(dateFormat.format(date));
	
	    boolean attendanceStatus=attendanceDao.employeeLogIn(attendance);
		 
		return attendanceStatus;
	}

}
